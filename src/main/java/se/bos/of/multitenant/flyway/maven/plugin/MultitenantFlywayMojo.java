package se.bos.of.multitenant.flyway.maven.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.util.Location;

/**
 * @author hans
 */
@Mojo(name = "migrate",
        defaultPhase = LifecyclePhase.NONE,
        requiresOnline = false, requiresProject = true,
        threadSafe = false)
public class MultitenantFlywayMojo extends AbstractMojo
{

  private static final String DB_URL = "db.url";
  private static final String DB_USER = "db.username";
  private static final String DB_PASSWORD = "db.password";
  private static final String LOCATIONS = "locations";
  private static final String JDBC_URL_TEMPLATE = "jdbc.url.template";
  private static final String JDBC_DRIVER_CLASSNAME = "jdbc.driver.classname";
  @Parameter(defaultValue = "${project}", required = true, readonly = true)
  private MavenProject mavenProject;
  @Parameter(property = "propertiesFile", required = false)
  protected File propertiesFile;
  @Parameter(property = "dbUrl", required = false)
  protected String dbUrl;
  @Parameter(property = "dbUsername", required = false)
  protected String dbUsername;
  @Parameter(property = "dbPassword", required = false)
  protected String dbPassword;
  @Parameter(property = "locations", required = false)
  protected String[] locations;
  @Parameter(property = "jdbcUrlTemplate", required = false)
  protected String jdbcUrlTemplate = "jdbc:mysql://%s:%d/%s";
  @Parameter(property = "jdbcDriverClassName", required = false)
  protected String jdbcDriverClassName; // = "com.mysql.jdbc.Driver";

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    getLog().info("Running multitenant-flyway plugin");
    setupAndCheckConfiguration();
    migrate();
  }

  private void migrate() throws MojoExecutionException {
    for (DataSource ds : getDataSources()) {
      Flyway flyway = new Flyway();
      flyway.setDataSource(ds);
      flyway.setInitOnMigrate(true);
      flyway.setLocations(locations);
      flyway.migrate();
    }
  }

  /**
   * The values in the properties file overrides pom settings.
   *
   * @throws MojoExecutionException
   */
  private void setupAndCheckConfiguration() throws MojoExecutionException {
    if (null != propertiesFile) {
      Properties props = new Properties();
      try {
        props.load(new BufferedReader(new FileReader(propertiesFile)));
      } catch (IOException ex) {
        throw new MojoExecutionException(String.format("Failed to read properties file at %s", propertiesFile.getAbsolutePath()));
      }
      dbUrl = props.getProperty(DB_URL, dbUrl);
      dbUsername = props.getProperty(DB_USER, dbUsername);
      dbPassword = props.getProperty(DB_PASSWORD, dbPassword);
      jdbcUrlTemplate = props.getProperty(JDBC_URL_TEMPLATE, jdbcUrlTemplate);
      jdbcDriverClassName = props.getProperty(JDBC_DRIVER_CLASSNAME, jdbcDriverClassName);
      if (!isEmpty(props.getProperty(LOCATIONS))) {
        locations = props.getProperty(LOCATIONS).split(",");
      }
    }
    if (isEmpty(dbUrl)) {
      throw new MojoExecutionException("No database URL specified either in pom.xml (\"dbUrl\") or property file (\"db.url\")");
    }
    if (isEmpty(dbUsername)) {
      throw new MojoExecutionException("No database username specified either in pom.xml (\"dbUsername\") or property file (\"db.username\")");
    }
    if ((locations != null) && (locations.length > 0)) {
      for (int i = 0; i < locations.length; i++) {
        if (locations[i].startsWith(Location.FILESYSTEM_PREFIX)) {
          String newLocation = locations[i].substring(Location.FILESYSTEM_PREFIX.length());
          File file = new File(newLocation);
          if (!file.isAbsolute()) {
            file = new File(mavenProject.getBasedir(), newLocation);
          }
          locations[i] = Location.FILESYSTEM_PREFIX + file.getAbsolutePath();
        }
      }
    } else {
      locations = new String[]{
        Location.FILESYSTEM_PREFIX + mavenProject.getBasedir().getAbsolutePath() + "/src/main/resources/db/migration"
      };
    }
  }

  private boolean isEmpty(String s) {
    return (null == s) || s.trim().isEmpty();
  }

  private List<DataSource> getDataSources() throws MojoExecutionException {
    List<DataSource> datasources = new ArrayList<>();
    // Let flyway create the customer bootstrap datasource (it will automatically detect and load the driver)
    Flyway flyway = new Flyway();
    flyway.setDataSource(dbUrl, dbUsername, dbPassword);
    DataSource mainDS = flyway.getDataSource();
    Connection conn = null;
    Statement stmt;
    ResultSet rs;

    try {
      conn = mainDS.getConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("SELECT jdbc_url, port, username, password_, schema_ FROM customer");
      while (rs.next()) {
        String jdbcUrl = String.format(jdbcUrlTemplate,
                rs.getString(1),
                rs.getInt(2),
                rs.getString(5)
        );
        getLog().debug(jdbcUrl);
        String user = rs.getString(3);
        String pass = rs.getString(4);
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(jdbcUrl);
        if (!isEmpty(jdbcDriverClassName)) {
          ds.setDriverClassName(jdbcDriverClassName);
        }
        ds.setUsername(user);
        ds.setPassword(pass);
        datasources.add(ds);
      }
    } catch (Exception ex) {
      throw new MojoExecutionException("Exception when creating tenant datasource: " + ex.getMessage());
    } finally {
      if (null != conn) {
        try {
          conn.close();
        } catch (SQLException ex) {
          // Ignore
        }
      }
    }
    return datasources;
  }
}
